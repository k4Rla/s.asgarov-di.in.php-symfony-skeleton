<?php
/**
 * @author Serdar Asgarov
 */

namespace Delodi\BookmarkBundle\Exception;

class InvalidApiKey extends \Exception {
    protected $code = 500;
    protected $message = "Invalid API_KEY passed";
} 