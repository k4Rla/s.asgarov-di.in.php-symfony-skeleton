<?php
/**
 * @author Serdar Asgarov
 *
 * Keywords. Entity.
 */
namespace Delodi\BookmarkBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Keyword
 */
class Keyword
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $bookmarkId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \Doctrine\ORM\PersistentCollection
     */
    private $bookmark;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookmarkId
     *
     * @param integer $bookmarkId
     * @return Keyword
     */
    public function setBookmarkId($bookmarkId)
    {
        $this->bookmarkId = $bookmarkId;

        return $this;
    }

    /**
     * Get bookmarkId
     *
     * @return integer 
     */
    public function getBookmarkId()
    {
        return $this->bookmarkId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Keyword
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
