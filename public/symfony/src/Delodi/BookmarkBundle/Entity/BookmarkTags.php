<?php
/**
 * @author Serdar Asgarov
 *
 * BookmarkTags. Entity.
 */
namespace Delodi\BookmarkBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BookmarkTags
 */
class BookmarkTags
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $bookmarkId;

    /**
     * @var integer
     */
    private $tagId;

    /**
     * @var \Doctrine\ORM\PersistentCollection
     */
    private $tag;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookmarkId
     *
     * @param integer $bookmarkId
     * @return BookmarkTags
     */
    public function setBookmarkId($bookmarkId)
    {
        $this->bookmarkId = $bookmarkId;

        return $this;
    }

    /**
     * Get bookmarkId
     *
     * @return integer 
     */
    public function getBookmarkId()
    {
        return $this->bookmarkId;
    }

    /**
     * Set tagId
     *
     * @param integer $tagId
     * @return BookmarkTags
     */
    public function setTagId($tagId)
    {
        $this->tagId = $tagId;

        return $this;
    }

    /**
     * Get tagId
     *
     * @return integer 
     */
    public function getTagId()
    {
        return $this->tagId;
    }

    /**
     * @return string
     */
    public function getTagName()
    {
        return $this->tag;//->getName();
    }
}
