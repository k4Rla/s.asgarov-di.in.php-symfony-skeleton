<?php
/**
 * @author Serdar Asgarov
 *
 * Bookmark. Entity.
 */
namespace Delodi\BookmarkBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Bookmark
 */
class Bookmark implements JsonSerializable
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $url;

    /**
     * @var boolean
     */
    private $readLater = true;

    /**
     * @var \Doctrine\ORM\PersistentCollection
     */
    private $tags;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \Doctrine\ORM\PersistentCollection
     */
    private $keywords;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get tags
     *
     * @return integer
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set id
     *
     * @param int $id
     * @return Bookmark
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Bookmark
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Bookmark
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set readLater
     *
     * @param boolean $readLater
     * @return Bookmark
     */
    public function setReadLater($readLater)
    {
        $this->readLater = $readLater;

        return $this;
    }

    /**
     * Get readLater
     *
     * @return boolean 
     */
    public function getReadLater()
    {
        return $this->readLater;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function jsonSerialize()
    {
        $tags = [];

        if (!empty($this->tags)) {
            $this->tags = $this->tags->getValues();
            /** @var \Delodi\BookmarkBundle\Entity\Tag $tag */
            foreach ($this->tags as $tag) {

                $tags[] = $tag->getName();
            }
        }

        $keywords = [];

        if (!empty($this->keywords)) {
            $this->keywords = $this->keywords->getValues();
            /** @var \Delodi\BookmarkBundle\Entity\Keyword $keyword */
            foreach ($this->keywords as $keyword) {

                $keywords[] = $keyword->getName();
            }
        }

        return array(
            'id'=> $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'url' => $this->url,
            'readLater' => $this->readLater,
            'tags' => $tags,
            'keywords' => $keywords
        );
    }


}
