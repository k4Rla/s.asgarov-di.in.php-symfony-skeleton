<?php
/**
 * @author Serdar Asgarov
 *
 * Tag. Entity.
 */
namespace Delodi\BookmarkBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Tag
 */
class Tag implements JsonSerializable
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    public function jsonSerialize()
    {
        return array(
            'id'=> $this->id,
            'name' => $this->name,
        );
    }
}
