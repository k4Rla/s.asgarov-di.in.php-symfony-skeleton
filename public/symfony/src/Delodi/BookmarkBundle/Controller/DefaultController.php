<?php

namespace Delodi\BookmarkBundle\Controller;

use Delodi\BookmarkBundle\Entity\Bookmark;
use Delodi\BookmarkBundle\Entity\BookmarkTags;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return new Response($this->postBookmark());
    }

    private function getBookmark($id = 1) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://62.76.188.58/app_dev.php/bookmarks/$id");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "api-key: 139da56de34e02cfe49e52a724eae966"));
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    private function updateBookmark() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://62.76.188.58/app_dev.php/bookmarks");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n        \"id\": 1,\n        \"title\": \"updated twice accepted delodi application\",\n        \"readLater\": false\n}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "api-key: 139da56de34e02cfe49e52a724eae966"));
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    private function postBookmark() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://62.76.188.58/app_dev.php/bookmarks");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n    \"title\": \"delodi application\",\n    \"url\": \"http://delodi.net/en/index.html\",\n    \"tags\": [\"delodi\", \"job\",\"application\"],\n    \"readLater\": true\n}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "api-key: 139da56de34e02cfe49e52a724eae966"));
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    private function deleteBookmark($id) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://62.76.188.58/app_dev.php/bookmarks/$id");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "api-key: 139da56de34e02cfe49e52a724eae966"));
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    private function search() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://62.76.188.58/app_dev.php/bookmarks?searchText=delodi&searchFields=[\"keywords\"]");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n    \"searchText\": \"delodi\",\n    \"searchFields\": [\"title\",\"description\"],\n    \"tags\": [\"delodi\",\"job\"],\n    \"count\": 1,\n    \"readLater\": true\n}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "api-key: 139da56de34e02cfe49e52a724eae966"));
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    private function tags() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://62.76.188.58/app_dev.php/tags");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "api-key: 139da56de34e02cfe49e52a724eae966"));
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}
