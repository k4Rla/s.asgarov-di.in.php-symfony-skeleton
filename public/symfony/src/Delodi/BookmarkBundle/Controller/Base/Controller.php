<?php
/**
 * @author Serdar Asgarov
 *
 * Controller. Base.
 *
 * * Service methods
 * @method checkKey() - checks wether the API KEY is valid
 * @method getJSONData  - returns provided json data
 * @method jsonResponse - responses correct json
 */

namespace Delodi\BookmarkBundle\Controller\Base;

use Delodi\BookmarkBundle\Exception\InvalidApiKey;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as SymfonyController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Controller extends SymfonyController {

    const API_KEY = "139da56de34e02cfe49e52a724eae966";

    /**
     * @param Request $request
     *
     * @throws \Delodi\BookmarkBundle\Exception\InvalidApiKey
     */
    protected function checkKey(Request $request) {
        $key = $request->headers->get('api-key');

        if (empty($key) || $key !== static::API_KEY) {
            throw new InvalidApiKey;
        }
    }

    /**
     * @return mixed
     */
    protected function getJSONData() {
        return json_decode($this->get("request")->getContent(), true);
    }

    /**
     * @param $data
     *
     * @return Response
     */
    protected function jsonResponse($data) {
        $data = json_encode( $data );
        $headers = array( 'Content-type' => 'application-json; charset=utf8' );

        return new Response( $data, 200, $headers );
    }
} 