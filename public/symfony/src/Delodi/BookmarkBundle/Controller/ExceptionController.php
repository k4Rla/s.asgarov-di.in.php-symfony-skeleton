<?php
/**
 * @author Serdar Asgarov
 *
 * Exception. Controller. Handles exceptions.
 *
 * * Actions
 *
 * @method showAction - shows json exception message
 */

namespace Delodi\BookmarkBundle\Controller;

use Delodi\BookmarkBundle\Controller\Base\Controller;
use Symfony\Component\Debug\Exception\FlattenException;


class ExceptionController extends Controller {

    /**
     * @param FlattenException $exception
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(FlattenException $exception) {
        return $this->jsonResponse(
            array(
                "code" => $exception->getCode(),
                "message" => $exception->getMessage(),
            )
        );
    }
} 