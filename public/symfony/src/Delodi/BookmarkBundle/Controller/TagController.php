<?php
/**
 * @author Serdar Asgarov
 *
 * Tag. Controller.
 *
 * * Actions
 * @method tagsAction - shows all tags in json
 *
 * * Other
 * @method findTags - returns tags from database
 */
namespace Delodi\BookmarkBundle\Controller;

use Delodi\BookmarkBundle\Entity\Tag;
use Delodi\BookmarkBundle\Controller\Base\Controller;
use Symfony\Component\HttpFoundation\Request;


class TagController extends Controller
{

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tagsAction(Request $request)
    {
        $this->checkKey($request);

        $result = array();

        $tags = $this->findTags();

        /** @var $tag Tag */
        foreach ($tags as $tag) {
            $result['tags'][] = $tag->getName();
        }

        return $this->jsonResponse($result);
    }

    /**
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return array
     */
    private function findTags() {
        $repository = $this->getDoctrine()->getRepository('DelodiBookmarkBundle:Tag');

        $result = $repository->findAll();

        if (empty($result)) {
            throw $this->createNotFoundException(
                'No tags found'
            );
        }

        return $result;
    }

}
