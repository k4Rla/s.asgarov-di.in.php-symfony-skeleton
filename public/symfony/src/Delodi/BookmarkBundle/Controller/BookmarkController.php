<?php
/**
 * @author Serdar Asgarov
 *
 * BookmarkController. Controller.
 *
 * * Actions
 * @method getAction - returns a bookmark with provided id
 * @method searchAction - returns bookmarks filtered by search query
 * @method createAction - creates new bookmark
 * @method updateAction - updates existing bookmark
 * @method deleteAction - deletes existing bookmark
 *
 * * Other methods
 *
 * @method search - searches through database using Doctrine
 * @method getBookmark - returns a single Bookmark from database
 * @method postBookmark - creates new Bookmark in database
 * @method updateBookmark - updates Bookmark in database
 * @method deleteBookmark - deletes Bookmark from database
 */
namespace Delodi\BookmarkBundle\Controller;

use Delodi\BookmarkBundle\Entity\Bookmark;
use Delodi\BookmarkBundle\Entity\BookmarkTags;
use Delodi\BookmarkBundle\Entity\Tag;
use Doctrine\ORM\QueryBuilder;
use Delodi\BookmarkBundle\Controller\Base\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class BookmarkController extends Controller
{
    /**
     * @param Request $request
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     *
     * @return Response
     */
    public function getAction(Request $request) {

        $this->checkKey($request);

        $id = $request->get('id');

        if (empty($id) || !is_numeric($id)) {
            throw new InvalidParameterException("Wrong 'id' parameter passed");
        }

        return $this->jsonResponse($this->getBookmark($id));
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function searchAction(Request $request) {
        $this->checkKey($request);

        return $this->jsonResponse($this->search($request));
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return Response
     */
    public function createAction(Request $request) {
        $this->checkKey($request);

        return $this->jsonResponse($this->postBookmark());
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return Response
     */
    public function updateAction(Request $request) {
        $this->checkKey($request);

        return $this->jsonResponse($this->updateBookmark());
    }

    /**
     * @param Request $request
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     *
     * @return Response
     */
    public function deleteAction(Request $request) {
        $this->checkKey($request);

        $id = $request->get('id');

        if (empty($id) || !is_numeric($id)) {
            throw new InvalidParameterException("Wrong 'id' parameter passed");
        }

        return $this->jsonResponse($this->deleteBookmark($id));
    }

    /**
     * @param Request $request
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     *
     * @return array
     */
    public function search(Request $request)
    {
        $searchText = $request->get('searchText');
        $searchFields = $request->get('searchFields', '["title"]');
        $tags = $request->get('tags');
        $offset = $request->get('offset', 0);
        $count = $request->get('count', 0);
        $readLater = $request->get('readLater');

        $em = $this->getDoctrine()->getRepository('DelodiBookmarkBundle:Bookmark');

        /** @var QueryBuilder $qb */
        $qb = $em->createQueryBuilder("qb");

        $qb->select("b")
            ->from("Delodi\BookmarkBundle\Entity\Bookmark", "b");

        if (!empty($searchText)) {

            $searchFields = json_decode($searchFields, true);

            if (!is_array($searchFields)) {
                throw new InvalidParameterException("Wrong 'searchFields' parameter passed; Must be json;");
            }

            $requests = [];

            foreach ($searchFields as $field) {
                if ($field == 'keywords') {
                    $qb->andWhere("k.name IN ('$searchText')");
                    $qb->leftJoin("b.keywords", "k");
                } else {

                    if (!property_exists((new Bookmark()), $field) || $field == 'tags' || $field == 'keywords') {
                        throw new InvalidParameterException("No such field '$field' in Bookmark;");
                    }

                    $requests[] = "b.$field LIKE '%$searchText%'";
                }
            }

            if (!empty($requests)) {
                $qb->andWhere(implode(" OR ", $requests));
            }
        }

        if (!empty($tags)) {
            $tags = json_decode($tags, true);

            if (!is_array($tags)) {
                throw new InvalidParameterException("Wrong 'tags' parameter passed; Must be json;");
            }

            $qb->andWhere("t.name IN ('" . implode(",", $tags) . "')");
            $qb->leftJoin("b.tags", "t");
        }

        if (!is_null($readLater)) {
            $readLater = (int) $readLater;
            $qb->andWhere("b.readLater = $readLater");
        }

        if ($count > 0) {

            $qb->setMaxResults((int) $count);

            if ($offset > 0) {
                $qb->setFirstResult($offset);
            }
        }

        $qb->groupBy("b.id");

        $q = $qb->getQuery();

        return array("results" => $q->getResult());

    }

    private function getBookmark($id) {
        $repository = $this->getDoctrine()->getRepository('DelodiBookmarkBundle:Bookmark');

        /** @var Bookmark $bookmark */
        $bookmark = $repository->findOneBy(array('id' => $id));

        if (empty($bookmark)) {
            throw $this->createNotFoundException(
                'No bookmark found for id ' . $id
            );
        }

        return $bookmark;
    }


    private function postBookmark() {
        $bookmark = new Bookmark();
        $json = $this->getJSONData();

        if (empty($json)) {
            throw new Exception("Nothing passed");
        }

        if (!empty($json['title'])) {
            $bookmark->setTitle($json['title']);
        }

        if (!empty($json['url'])) {
            $bookmark->setUrl($json['url']);
        }

        if (is_bool($json['readLater'])) {
            $bookmark->setReadLater($json['readLater']);
        }

        $em = $this->getDoctrine()->getManager();

        $em->persist($bookmark);
        $em->flush();

        if (!empty($json['tags'])) {
            $repository = $this->getDoctrine()->getRepository('DelodiBookmarkBundle:Tag');

            $tags = $repository->findBy(array(
                "name" => $json['tags']
            ));

            /** @var Tag $tag */
            foreach ($tags as $tag) {
                $bookmarkTags = new BookmarkTags();

                $bookmarkTags->setBookmarkId($bookmark->getId());
                $bookmarkTags->setTagId($tag->getId());

                $em = $this->getDoctrine()->getManager();
                $em->persist($bookmarkTags);

                $em->flush();
            }
        }

        return $this->getBookmark($bookmark->getId());
    }

    private function updateBookmark() {
        $json = $this->getJSONData();

        $id = $json['id'];

        if (empty($id) || !is_numeric($id)) {
            throw new InvalidParameterException("Wrong id parameter passed");
        }

        $em = $this->getDoctrine()->getManager();
        $bookmark = $em->getRepository('DelodiBookmarkBundle:Bookmark')->find($id);

        if (empty($bookmark)) {
            throw new Exception("Bookmark not found");
        }


        if (!empty($json['title'])) {
            $bookmark->setTitle($json['title']);
        }

        if (!empty($json['url'])) {
            $bookmark->setUrl($json['url']);
        }

        if (is_bool($json['readLater'])) {
            $bookmark->setReadLater($json['readLater']);
        }

        $em->flush();

        if (!empty($json['tags'])) {

            $query = $em->createQuery("DELETE FROM \Delodi\BookmarkBundle\Entity\BookmarkTags t WHERE t.bookmarkId = " . $id);
            $query->execute();

            $repository = $this->getDoctrine()->getRepository('DelodiBookmarkBundle:Tag');

            $tags = $repository->findBy(array(
                "name" => $json['tags']
            ));

            /** @var Tag $tag */
            foreach ($tags as $tag) {
                $bookmarkTags = new BookmarkTags();

                $bookmarkTags->setBookmarkId($bookmark->getId());
                $bookmarkTags->setTagId($tag->getId());

                $em = $this->getDoctrine()->getManager();
                $em->persist($bookmarkTags);

                $em->flush();
            }

        }

        return $bookmark;
    }

    private function deleteBookmark($id) {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("DELETE FROM \Delodi\BookmarkBundle\Entity\BookmarkTags t WHERE t.bookmarkId = " . $id);
        $query->execute();

        $query = $em->createQuery("DELETE FROM \Delodi\BookmarkBundle\Entity\Bookmark b WHERE b.id = " . $id);
        $query->execute();

        return array( "message" => "bookmark deleted successfully" );
    }

}